package controllers

import (
	"github.com/gin-gonic/gin"
	"nationalCode/services"
	"net/http"
)

// Validate National Code godoc
// @Summary Get Iranian Person National Code Validate And Return City Name
// @Description Get Iranian Person National Code Validate And Return City Name
// @Tags NationalCode
// @ID validate-national-code
// @Accept  json
// @Produce  html
// @Param id ٰquery string true "National Code"
// @Header 200 {string} Token "qwerty"
// @Router /ncode [get]
func ValidateNationalCode(c *gin.Context) {


	if nationalCode := c.Query("id");nationalCode!="" {

			var (
				valid    bool
				cityName string
				errorCity error
			)

			nationalCodeObject, err := services.NewNationalCode(nationalCode)

			if err == nil {
				valid = true
				cityName, errorCity = nationalCodeObject.GetCityName()
			}

			if errorCity != nil || err!=nil {
				cityName = "تعریف نشده"
			}

		   c.JSON(http.StatusOK,gin.H{"id":nationalCode,"isValid":valid,"city":cityName})

	}else {
		c.JSON(http.StatusNotAcceptable,"Id is required")
	}


}
