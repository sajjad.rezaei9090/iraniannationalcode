package main

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	_"nationalCode/docs"
)

var router *gin.Engine

// @title Web Service API Validate National Code
// @version 1.0
// @description This is Validate Iranian National Code web service  Golang

// @contact.name sajjad
// @contact.url
// @contact.email sajjad.rezaei9090@gmail.com

// @host 127.0.0.1:8080
// @BasePath
// @query.collection.format multi
func main()  {

	router=gin.Default()


	// Initialize the routes
	 initRoutes()

	//url := ginSwagger.URL("127.0.0.1:8080/swagger/doc.json") // The url pointing to API definition
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))


	// Start serving the application
	router.Run(":8080")

}
