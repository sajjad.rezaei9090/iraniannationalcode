package services

import "errors"

var cityCode map[string]string = map[string]string{
	"412": "بروجرد",
	"413": "بروجرد",
	"406": "خرم اباد",
	"407": "خرم اباد",
	"421": "دورود",
	"498": "بابلسر",
	"483": "چالوس",
	"227": "رامسر",
	"627": "کلاردشت",
}

func getCityName(code string) (string, error) {

	val, ok := cityCode[code]

	if ok {
		return val, nil
	}

	return "", errors.New("city not found")
}
