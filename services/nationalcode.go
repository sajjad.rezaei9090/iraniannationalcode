package services
import (
	"errors"
	"strconv"
)

var placeValue = [10]int{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}

const residulaConst = 11

const controlDigitIndex = 9

type NationalCode struct {
	stringFormat string
	digitFormat  [10]int
	cityCode     string
	IsValid      bool
}
// validate new national code
func NewNationalCode(id string) (*NationalCode, error) {

	var err error

	if id == "" {
		return nil, errors.New("National Id is empty")
	}

	nId := NationalCode{stringFormat: id}

	nId.IsValid, err = nId.validation()

	if err != nil {
		return nil, err
	}
	return &nId, nil

}

func (id *NationalCode) validation() (bool, error) {

	if len(id.stringFormat) != 10 {
		return false, errors.New("national code is not valid")
	}

	for index, val := range id.stringFormat {

		digit, err := strconv.Atoi(string(val))

		if err != nil {
			return false, err
		}

		id.digitFormat[index] = digit
	}
	ret, err := id.isCheckSumValid()

	if err != nil || ret == false {
		return false, err
	}

	id.IsValid = true
	id.cityCode = id.stringFormat[0:3]
	return true, nil

}

func (id *NationalCode) GetCityName() (string, error) {
	return getCityName(id.cityCode)
}

func (id *NationalCode) isCheckSumValid() (bool, error) {

	var sum, resid, controlDigit int

	for index := 0; index < 9; index++ {
		sum = sum + id.digitFormat[index]*placeValue[index]

	}

	resid = sum % residulaConst

	controlDigit = id.digitFormat[controlDigitIndex]

	if resid < 2 && resid == controlDigit {
		return true, nil
	}

	if controlDigit == (11 - resid) {
		return true, nil
	}

	return false, errors.New("CheckSum Is in Valid")

}